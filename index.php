<?php
    
    $cityId = 620127;
    $appKey = '3a1183984b3705860250021948568fb6';
    $cacheFilename = "cache.cache";
    $expiry = 3600;   

    if (!file_exists($cacheFilename) or (time() - $expiry) > filemtime($cacheFilename)) {
        $content = file_get_contents("http://api.openweathermap.org/data/2.5/weather?id=$cityId&appid=$appKey");   
        $fp = fopen($cacheFilename, 'w');
        fwrite($fp, $content);
        fclose($fp); 
        $message = "Погода получена с сайта";  
    } else {
        $content = file_get_contents($cacheFilename);
        $message = "Погода получена из КЕША";
    }   
    
    $result = json_decode($content, true);
    // echo "<pre>";
    // print_r($result);
    $temp = round($result['main']['temp'] - 273.15, 1);
    $date = $result['dt'];
    $description = $result['weather'][0]['description'];
    $humidity = $result['main']['humidity'];
    $pressure = $result['main']['pressure'];
    $icon = $result['weather'][0]['icon'];
    $date_format = date('d.m.Y', $date);
    $name = $result['name'];    
   
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Weather in <?= $name ?></title>
</head>
<body>
    <div style="width: 500px; margin: 0 auto; text-align: center">    
        <h1 style="margin-bottom: 0">Weather in <?= $name . " " . $date_format ?></h1>
        <p style="margin: 0;">(<?= $message ?>)</p>
        <img style="margin-left: -50px" src="https://openweathermap.org/img/w/<?= $icon ?>.png" alt="icon">
        <p style="margin-top: 0px; font-size: 20px">Now in <?= $name ?> <?= $temp. "&deg;C, <br>" . $description . ", <br>" . "humidity" . " " . "$humidity%," . " " . "pressure $pressure hpa"  ?></p>        
    </div>
</body>
</html>
